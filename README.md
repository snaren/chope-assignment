**Please note**

1. The server is already running for you on aws cloud and can be reached at url : 

	`http://http://ec2-54-255-165-196.ap-southeast-1.compute.amazonaws.com:8080`

2. If you see any secret info on web UI between two colons `:<msg here>:` that's because it's been put intentionally only for test purpose

3. Below functionalities are suppoerted

```

	- Index page : http://ec2-54-255-165-196.ap-southeast-1.compute.amazonaws.com:8080/v1/index
	
	    |
		 - Register (POST http://ec2-54-255-165-196.ap-southeast-1.compute.amazonaws.com:8080/v1/register)
		 		|
				 - Enter details and click submit
		|
		 - Loging (POST http://ec2-54-255-165-196.ap-southeast-1.compute.amazonaws.com:8080/v1/login)
		 		|
				 -  Enter credentials and click login
		|
		 - Login by google
		 		|
				 - Click on google login icon, Currently google LOGOUT is not suppoeted due to configuration issues
		|
		 - Home (GET http://ec2-54-255-165-196.ap-southeast-1.compute.amazonaws.com:8080/v1/home)
		 		|
				 - Logout
				|
				 - View login activity
```
				 
4. Due to configure issues, the verification of google IDToken on backend server is skipped for now.
5. Due to google LOGOUT NOT supported, if you login by google you would still stay on index screen. Please click on Home to view the login protected screens.
6. To remove google login,just cleat your browser cookkies.

