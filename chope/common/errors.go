package common

import "net/http"

type ApiError struct {
	Code int    `json:"code"`
	Msg  string `json:"message"`
}

func (e ApiError) Error() string {
	return e.Msg
}

func ErrorInternalServer(msg string) *ApiError {
	return &ApiError{
		Code: http.StatusInternalServerError,
		Msg:  msg,
	}
}

func ErrorBadRequest(msg string) *ApiError {
	return &ApiError{
		Code: http.StatusBadRequest,
		Msg:  msg,
	}
}

func ErrorNotFound(msg string) *ApiError {
	return &ApiError{
		Code: http.StatusNotFound,
		Msg:  msg,
	}
}

func ErrorForbidden(msg string) *ApiError {
	return &ApiError{
		Code: http.StatusForbidden,
		Msg:  msg,
	}
}

func ErrorConflict(msg string) *ApiError {
	return &ApiError{
		Code: http.StatusConflict,
		Msg:  msg,
	}
}
