package server

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/tylerb/graceful"

	"bitbucket/go_chope/chope-assignment/chope/api"
	"bitbucket/go_chope/chope-assignment/chope/config"
	"bitbucket/go_chope/chope-assignment/chope/dto"
	"bitbucket/go_chope/chope-assignment/chope/storage"
	"bitbucket/go_chope/chope-assignment/commons/gateway"
)

func RegisterRoutes(v1 *mux.Router) {
	fmt.Println("registering routes...")

	/**************************************************
	******************* Login/Register APIs ***********
	***************************************************/
	v1.HandleFunc("/index", gateway.HandleWeb(api.Index, &dto.UserLogin{})).Methods(http.MethodGet)
	v1.HandleFunc("/login", gateway.HandleWeb(api.LoginWeb, &dto.UserLogin{})).Methods(http.MethodPost)
	v1.HandleFunc("/google-login", gateway.HandleWeb(api.GoogleLogin, &dto.GoogleLogin{})).Methods(http.MethodPost)

	v1.HandleFunc("/login/activities", gateway.HandleWebWithLogin(api.LoginActivity, &dto.UserLogin{})).Methods(http.MethodGet)
	v1.HandleFunc("/home", gateway.HandleWebWithLogin(api.Home, &dto.UserLogin{})).Methods(http.MethodGet)
	v1.HandleFunc("/register", gateway.HandleWeb(api.RegisterUser, &dto.UserRegister{})).Methods(http.MethodPost)
	v1.HandleFunc("/register/user", gateway.HandleWeb(api.RegisterUserWeb, &dto.UserRegister{})).Methods(http.MethodPost)
	v1.HandleFunc("/logout", gateway.HandleWebWithLogin(api.LogoutWeb, &dto.UserLogout{})).Methods(http.MethodPost)

	fmt.Println("registering routes...completed")
}

func StartServer(handler http.Handler) {
	log.Printf("starting server... version:%s", config.WerbServerVersion)
	srv := &graceful.Server{
		Timeout: 5 * time.Second,
		Server: &http.Server{
			Addr:    config.Config.Server.Host + ":" + config.Config.Server.Port,
			Handler: handler,
		},
	}

	storage.InitUserDao(&config.Config.Mysql)

	if err := srv.ListenAndServe(); err != nil {
		log.Println("http server error while listening, err=", err)
	}
}
