package storage

import (
	"context"
	"log"
	"reflect"
	"time"

	"bitbucket/go_chope/chope-assignment/chope/config"
	"bitbucket/go_chope/chope-assignment/commons/mysql"
	usertype "bitbucket/go_chope/chope-assignment/commons/usertypes"
)

type User struct {
	ID                 int64             `sql-col:"id" sql-insert:"false" sql-update:"false" `
	FirstName          string            `sql-col:"first_name"`
	LastName           string            `sql-col:"last_name"`
	Email              string            `sql-col:"email" sql-update:"false"`
	Mobile             string            `sql-col:"mobile"`
	UserType           usertype.UserType `sql-col:"user_type"`
	Password           string            `sql-col:"password"`
	TempPassword       string            `sql-col:"temp_password"`
	TempPasswordExpire time.Time         `sql-col:"temp_password_expires" sql-insert:"false"`
	IsAccountVerified  bool              `sql-col:"is_account_verified"`
	IsEmailVerified    bool              `sql-col:"is_email_verified"`
	//  TODO: add new banned flag to block specific users
	//IsBanned            bool            `sql-col:"is_email_verified"`
	EmailVerificaionCode       string    `sql-col:"email_verification_code"`
	EmailVerificationSentAt    time.Time `sql-col:"email_verification_sent" sql-insert:"false"`
	EmailVerificationExpiresAt time.Time `sql-col:"email_verification_expires" sql-insert:"false"`
	EmailVerifiedAt            time.Time `sql-col:"email_verified_at" sql-insert:"false"`
	IsGoogleSignUp             int64     `sql-col:"is_google_signup"`
	UserAgent                  string    `sql-col:"user_agent"`
	Avatar                     string    `sql-col:"avatar"`
	PostScript                 string    `sql-col:"post_script"`
	CreatedAt                  time.Time `sql-col:"created_at" sql-insert:"false" sql-update:"false"`
	UpdatedAt                  time.Time `sql-col:"updated_at" sql-insert:"false" sql-update:"false"`
}

type UserDao_I interface {
	// FindOne returns only object. if there are muliple records exist, it retunrs the most recent one
	FindOne(ctx context.Context, conditions ...mysql.Condition) (*User, error)
	// FindList returns list of objects
	FindList(ctx context.Context, conditions ...mysql.Condition) ([]*User, error)
	// Save inserts the new records. It returns the id of inserted record
	Save(ctx context.Context, obj *User) (int64, error)
	// Update updates the existing record using optimistic lock
	UpdateByCondition(old, new *User) error
	// FindByID returns the record by ID
	FindByID(ctx context.Context, id int) (*User, error)
}

func (u *User) GetTableName() string {
	return "users"
}

func (u *User) New() mysql.Entity {
	return &User{}
}

type UserDao_T struct {
	mysqlMgr *mysql.MySqlMgr
}

var UserDao UserDao_T

func InitUserDao(conf *config.MysqlConfig) {
	config := &mysql.MysqlConnInfo{
		Host:     conf.Host,
		Port:     conf.Port,
		User:     conf.User,
		Password: conf.Password,
		DbName:   conf.DbName,
	}
	log.Printf("setting config=%+v", config)
	UserDao.mysqlMgr = mysql.GetDao(config, &User{})
}

func (dao *UserDao_T) UpdateByCondition(oldUser, newUser *User) error {
	err := dao.mysqlMgr.UpdateNew(oldUser, newUser)
	if err != nil {
		log.Printf("failed to update to DB, entity=User, err=%+v", err)
		return err
	}
	return nil
}

func (dao *UserDao_T) Save(user *User) (int64, error) {
	id, err := dao.mysqlMgr.Save(user)
	if err != nil {
		log.Printf("failed to save to DB, entity=User, err=%+v", err)
		return 0, err
	}
	return id, nil
}

func (dao *UserDao_T) FindListConditions(conditions ...mysql.Condition) ([]*User, error) {
	users, err := dao.mysqlMgr.FindListConditions(conditions...)
	if err != nil {
		log.Printf("failed to find appointment by userID, err=%+v", err)
		return nil, err
	}
	log.Printf("... err=%+v", err)

	userList := []*User{}
	for _, item := range users {
		user, ok := item.(*User)
		if !ok {
			log.Printf("unexpected obj received for entity, expected=%v, received=%+v", "*User", reflect.TypeOf(user))
			return nil, err
		}
		userList = append(userList, user)
	}

	return userList, nil
}
