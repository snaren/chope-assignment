package api

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	"golang.org/x/crypto/bcrypt"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"

	"bitbucket/go_chope/chope-assignment/chope/config"
	"bitbucket/go_chope/chope-assignment/chope/dto"
	"bitbucket/go_chope/chope-assignment/chope/logic/usermgr"
	"bitbucket/go_chope/chope-assignment/chope/storage"
	"bitbucket/go_chope/chope-assignment/commons/gateway"
	"bitbucket/go_chope/chope-assignment/commons/mysql"
	usertype "bitbucket/go_chope/chope-assignment/commons/usertypes"
	"bitbucket/go_chope/chope-assignment/commons/uuidgo"
)

type loginActivity struct {
	LoginTime       int64
	SessionDuration int64
}

type loginActivityRecord struct {
	LoginTime       string
	SessionDuration string
}

func LoginWeb(w http.ResponseWriter, r *http.Request, req gateway.ReqValidator) (*template.Template, interface{}) {
	request := req.(*dto.UserLogin)
	log.Printf("UserLogin... req=%+v", request)

	request.Email = r.FormValue("email")
	request.Password = r.FormValue("password")

	data := &dto.UserLoginResponse{
		ErrorMessage: "Please try to login again",
	}
	resp := &dto.UserLoginResponse{
		Code:    http.StatusForbidden,
		Message: "Account not verified yet. Please check your email and follow instructiont to verify email.",
	}

	user, errUser := usermgr.GetUserByEmail(request.Email)
	if errUser != nil {
		if errUser == mysql.ErrNoData {
			resp.Code = http.StatusUnauthorized
			data.ErrorMessage = "Invalid email or password"
			t := template.Must(template.ParseFiles(loginErrorTemplate))
			return t, data

		}
	}

	// Comparing the password with the hash
	errMatch := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(request.Password))
	if errMatch != nil {
		// Comparing the password with the hash
		if user.TempPassword != request.Password || user.TempPasswordExpire.UTC().Before(time.Now().UTC()) {
			log.Printf("password mismatch, user email=%v", request.Email)
			data.ErrorMessage = "Invalid email or password"
			t := template.Must(template.ParseFiles(loginErrorTemplate))
			return t, data

		}
	}

	// This is to support multiple user type
	userType := usertype.UTRegular
	session, err := gateway.GetStore().Get(r, gateway.GetStoreNameByUserType(userType))
	if err != nil {
		log.Printf("failed to get session, userType=%v, err=%+v", user.UserType, err)
		data.ErrorMessage = "Server error:Failed to get session:please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data

	}

	session.Values[keyUserID] = user.ID
	session.Values[keyUserType] = int64(user.UserType)
	session.Values[keyAuthenticated] = true
	session.Values[keyLoggedInSince] = time.Now().UTC().Unix()
	if session.IsNew {
		log.Printf("new session...")
	}

	encode, err := securecookie.EncodeMulti(session.Name(), session.Values, gateway.GetStore().Codecs...)
	if err != nil {
		log.Printf("encode cookie failed, err=%+v", err)
		data.ErrorMessage = "Server error:Encode cokkie failed:please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	_ = sessions.NewCookie(session.Name(), encode, session.Options)
	err = session.Save(r, w)
	if err != nil {
		log.Printf("failed to save session, err=%+v", err)
		data.ErrorMessage = "Server error:Failed to dave session:please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	data.ErrorMessage = "You've logged in successfully."
	t := template.Must(template.ParseFiles(loginSuccessTemplate))
	return t, data
}

func GoogleLogin(w http.ResponseWriter, r *http.Request, req gateway.ReqValidator) (*template.Template, interface{}) {
	request := req.(*dto.GoogleLogin)

	log.Printf("GoogleLogin... req=%+v", request)

	data := &dto.UserLoginResponse{
		ErrorMessage: "Please try to login again",
	}

	authCode := r.Header.Get("auth_code") // auth_code is idToken received from google sdk on app
	if len(authCode) <= 0 {
		log.Printf("empty header auth_code")
		data.ErrorMessage = "Missing google loging ID token"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	if config.FireBaseClient == nil {
		log.Printf("nil firebase sdk in config. failed to complete google login. email=%s, idtoken=%s", request.Email, authCode)
		data.ErrorMessage = "Server error, please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	googleProfile := config.FireBaseClient.VerifyToken(authCode, request.Email)
	if googleProfile == nil {
		log.Println("failed to get user profile from google")
		data.ErrorMessage = "Server error:Failed to get google profile: please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	log.Printf("gProfile=%v", googleProfile)

	if len(googleProfile.Email) <= 0 {
		log.Println("empty user email from google")
		data.ErrorMessage = "Server error:missing email from google profile: please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	userID := int64(0)
	userType := usertype.UTRegular

	user, err := usermgr.GetUserByEmail(googleProfile.Email)
	if err != nil && err != mysql.ErrNoData {
		log.Printf("db error while searching user, err=%+v", err)
		data.ErrorMessage = "Server error:db error: please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	if err == mysql.ErrNoData {
		registerReq := &dto.UserRegister{
			MsgID:            uuidgo.NewUUID(),
			FirstName:        googleProfile.FirstName,
			LastName:         googleProfile.LastName,
			Email:            googleProfile.Email,
			UserType:         usertype.UTRegular,
			Avatar:           googleProfile.Picture,
			IsEmailVerfied:   googleProfile.IsEmailVerified,
			IsThirpartyLogin: true,
		}
		log.Printf("register google user in google login, email=%v", googleProfile.Email)

		resp, err := usermgr.Register(registerReq, false)
		if err != nil {
			log.Printf("failed to register google user in google login flow, err=%+v", err)
			data.ErrorMessage = "Server error:db error: please try again"
			t := template.Must(template.ParseFiles(loginErrorTemplate))
			return t, data
		}
		userID = resp.UserID
		userType = usertype.UTRegular
	} else {
		userInfoNew := *user
		userID = user.ID
		userType = user.UserType
		// TODO Update verification status
		if user.IsEmailVerified != googleProfile.IsEmailVerified {
			if !user.IsEmailVerified {
				userInfoNew.IsEmailVerified = googleProfile.IsEmailVerified
			}
			err = storage.UserDao.UpdateByCondition(user, &userInfoNew)
			if err != nil {
				log.Printf("failed to update user avatar in login, err=%+v", err)
				data.ErrorMessage = "Server error:db error: please try again"
				t := template.Must(template.ParseFiles(loginErrorTemplate))
				return t, data
			}
		}
	}

	session, err := gateway.GetStore().Get(r, gateway.GetStoreNameByUserType(userType))
	if err != nil {
		log.Printf("failed to get session, userType=%v, err=%+v", userType, err)
		data.ErrorMessage = "Server error:failed to get session: please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	session.Values[keyUserID] = userID
	session.Values[keyUserType] = int64(userType)
	session.Values[keyAuthenticated] = true
	if session.IsNew {
		log.Printf("new session...")
	}

	encode, err := securecookie.EncodeMulti(session.Name(), session.Values, gateway.GetStore().Codecs...)
	if err != nil {
		log.Printf("encode cookie failed, err=%+v", err)
		data.ErrorMessage = "Server error, please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	_ = sessions.NewCookie(session.Name(), encode, session.Options)
	err = session.Save(r, w)
	if err != nil {
		log.Printf("failed to save session, err=%+v", err)
		data.ErrorMessage = "Server error, please try again"
		t := template.Must(template.ParseFiles(loginErrorTemplate))
		return t, data
	}

	log.Printf("loginnnn session...")
	return template.Must(template.ParseFiles(homeTemplate)), data
}

func LogoutWeb(w http.ResponseWriter, r *http.Request, req gateway.ReqValidator) (*template.Template, interface{}) {
	request := req.(*dto.UserLogout)
	log.Printf("UserLogout req=%+v", request)

	data := &dto.UserLoginResponse{
		ErrorMessage: "",
	}

	ut := usertype.UTRegular
	session, err := gateway.GetStore().Get(r, gateway.GetStoreNameByUserType(ut))
	if err != nil {
		log.Printf("failed to get session, err=%+v", err)
		data.ErrorMessage = "Server error:failed to get session: please try again"
		t := template.Must(template.ParseFiles(indexTemplate))
		return t, data
	}

	authed, authOk := session.Values[keyAuthenticated]

	if session.IsNew || !authOk || !authed.(bool) {
		log.Printf("session is new or user not logged in, session=%+v", session)
		t := template.Must(template.ParseFiles(indexTemplate))
		return t, nil
	}

	userid := session.Values[keyUserID].(int64)
	session.Values[keyUserID] = 0
	session.Values[keyAuthenticated] = false
	session.Values[keyUserType] = int64(usertype.UTInvalid)

	err = session.Save(r, w)
	if err != nil {
		log.Printf("failed to save session, err=%+v", err)
		data.ErrorMessage = "Server error:failed to get session: please try again"
		t := template.Must(template.ParseFiles(loginSuccessTemplate))
		return t, data
	}

	loginTime := time.Unix(session.Values[keyLoggedInSince].(int64), 0)
	activity := &loginActivity{
		LoginTime:       session.Values[keyLoggedInSince].(int64),
		SessionDuration: int64(time.Since(loginTime).Seconds()),
	}

	key := fmt.Sprintf("login_activity:user_id:%d", userid)

	val, errJson := json.Marshal(activity)
	if errJson != nil {
		log.Printf("failed to encode login activity, err=%+v", errJson)
		// Failed to save activity just redirect user to login screen
	} else {
		err := config.RedisClient.LPush(context.Background(), key, string(val))
		if err != nil {
			log.Printf("failed to add login activity to redis, err=%v", err)
			// Failed to save activity just redirect user to login screen
		}
	}

	tmpl := template.Must(template.ParseFiles(indexTemplate))
	return tmpl, nil
}

func LoginActivity(w http.ResponseWriter, r *http.Request, req gateway.ReqValidator) (*template.Template, interface{}) {
	request := req.(*dto.UserLogin)
	log.Printf("UserLogin... req=%+v", request)

	data := &dto.UserLoginResponse{
		ErrorMessage: "",
	}

	ut := usertype.UTRegular
	session, err := gateway.GetStore().Get(r, gateway.GetStoreNameByUserType(ut))
	if err != nil {
		log.Printf("failed to get session, err=%+v", err)
		data.ErrorMessage = "Please login again"
		t := template.Must(template.ParseFiles(indexTemplate))
		return t, data
	}

	authed, authOk := session.Values[keyAuthenticated]

	if session.IsNew || !authOk || !authed.(bool) {
		log.Printf("session is new or user not logged in, session=%+v", session)
		data.ErrorMessage = "Please login again"
		t := template.Must(template.ParseFiles(indexTemplate))
		return t, data
	}

	key := fmt.Sprintf("login_activity:user_id:%d", session.Values[keyUserID].(int64))

	redisData, err := config.RedisClient.LRange(context.Background(), key, 0, 10)
	if err != nil {
		log.Printf("failed to add login activity to redis, err=%v", err)
		data.ErrorMessage = "Server error: failed to get data from redis:Please try again"
		t := template.Must(template.ParseFiles(loginSuccessTemplate))
		return t, data
	}
	log.Printf("activity data from redis=%+v", redisData)

	activitiesOut := []loginActivityRecord{}
	for _, item := range redisData {
		d := &loginActivity{}
		err = json.Unmarshal([]byte(item), d)
		if err != nil {
			log.Printf("failed to parse json activities from redis, err=%+v", err)
			// Continue to process other records
			continue
		}

		duration := ""
		seconds := d.SessionDuration % 60
		mins := d.SessionDuration / 60
		hours := mins / 60

		if hours > 0 {
			duration = fmt.Sprintf("%d hour", hours)
		}
		if mins > 0 {
			duration += fmt.Sprintf(" %d min", mins)
		}
		if seconds > 0 {
			duration += fmt.Sprintf(" %d sec", seconds)
		}

		activitiesOut = append(activitiesOut, loginActivityRecord{
			LoginTime:       time.Unix(d.LoginTime, 0).String(),
			SessionDuration: duration,
		})
	}
	log.Printf("activity data=%+v", activitiesOut)

	t := template.Must(template.ParseFiles(loginActivityTemplate))
	return t, activitiesOut
}
