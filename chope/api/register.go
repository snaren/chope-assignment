package api

import (
	"html/template"
	"log"
	"net/http"

	"bitbucket/go_chope/chope-assignment/chope/dto"
	"bitbucket/go_chope/chope-assignment/chope/logic/usermgr"
	"bitbucket/go_chope/chope-assignment/commons/gateway"
	usertype "bitbucket/go_chope/chope-assignment/commons/usertypes"
)

const (
	key               = "ea16935250b911ea8d772e728ce88125"
	storeUsers        = "session-user"
	storeProviders    = "session-provider"
	storePartnerUsers = "session-partner-user"
	storePartners     = "session-partner"

	keyUserID           = "userID"
	keyUserType         = "userType"
	keyAuthenticated    = "authenticated"
	keyLoggedInSince    = "loggedInSince"
	keyLoggedInActivity = "loginActivity"
)

func RegisterUser(w http.ResponseWriter, r *http.Request, req gateway.ReqValidator) (*template.Template, interface{}) {
	tmpl := template.Must(template.ParseFiles("./templates/register.html"))
	return tmpl, nil
}
func RegisterUserWeb(w http.ResponseWriter, r *http.Request, req gateway.ReqValidator) (*template.Template, interface{}) {
	request := req.(*dto.UserRegister)
	request.Email = r.FormValue("email")
	request.Password = r.FormValue("password")
	request.UserType = usertype.UTRegular
	log.Printf("RegisterUser Req=%+v", request)

	data := &dto.GenericResponse{
		ErrorMessage: "You've registered successfully",
	}

	resp, err := usermgr.Register(request, false)
	if resp != nil {
		resp.UserID = 0
		data.ErrorMessage = "Server error:" + err.Error() + ": please try again"
		tmpl := template.Must(template.ParseFiles(registerTemplate))
		return tmpl, data

	}

	tmpl := template.Must(template.ParseFiles(indexTemplate))
	return tmpl, data
}
