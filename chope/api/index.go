package api

import (
	"html/template"
	"net/http"

	"bitbucket/go_chope/chope-assignment/commons/gateway"
)

const (
	indexTemplate         = "./templates/index.html"
	homeTemplate          = "./templates/login_success.html"
	registerTemplate      = "./templates/register.html"
	loginSuccessTemplate  = "./templates/login_success.html"
	loginErrorTemplate    = "./templates/login_error.html"
	loginActivityTemplate = "./templates/login_activity.html"
)

func Index(w http.ResponseWriter, r *http.Request, req gateway.ReqValidator) (*template.Template, interface{}) {
	tmpl := template.Must(template.ParseFiles("./templates/index.html"))
	return tmpl, nil
}
