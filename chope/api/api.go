package api

import (
	"encoding/json"
	"log"
	"net/http"

	"bitbucket/go_chope/chope-assignment/chope/common"
)

func generateHttpResponse(resp interface{}, err interface{}) ([]byte, int) {
	if e, ok := err.(*common.ApiError); (ok && e == nil) || err == nil {
		log.Printf("generateHttpResponse, err nil")
		jsonResp, errr := json.Marshal(resp)
		if string(jsonResp) == "null" {
			log.Printf("generateHttpResponse, success, No content, errr=%+v", errr)
			return nil, http.StatusNoContent
		}
		// log.Printf("generateHttpResponse, jsonResp=%s, errr=%+v", jsonResp, errr)
		return jsonResp, http.StatusOK
	}

	if e, ok := err.(*common.ApiError); ok {
		log.Printf("err type =*common.ApiError, val=%+v, %+v", e, ok)
		jsonResp, _ := json.Marshal(e)
		return jsonResp, e.Code
	}

	if e, ok := err.(string); ok {
		jsonResp, _ := json.Marshal(common.ApiError{Code: http.StatusBadRequest, Msg: e})
		return jsonResp, http.StatusBadRequest
	}

	return nil, http.StatusBadRequest
}
