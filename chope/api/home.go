package api

import (
	"html/template"
	"net/http"

	"bitbucket/go_chope/chope-assignment/chope/dto"
	"bitbucket/go_chope/chope-assignment/commons/gateway"
)

func Home(w http.ResponseWriter, r *http.Request, req gateway.ReqValidator) (*template.Template, interface{}) {
	data := &dto.GenericResponse{
		ErrorMessage: "You're currently logged in",
	}
	tmpl := template.Must(template.ParseFiles(homeTemplate))
	return tmpl, data
}
