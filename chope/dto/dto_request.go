package dto

import (
	"fmt"
	"strings"

	public "bitbucket/go_chope/chope-assignment/commons/usertypes"
)

type BaseLoginReq struct {
	MsgID string `json:"msgID"`
	Email string `json:"email"`
}

func (req *BaseLoginReq) GetError() []error {
	err := []error{}
	if e := validateStringWithLen(req.Email, "email", MaxEmailLen); e != nil {
		err = append(err, e)
	}
	req.Email = strings.ToLower(req.Email)
	return err
}

func (req *BaseLoginReq) String() string {
	out := fmt.Sprintf("msgID=%v", req.MsgID)
	out += fmt.Sprintf(", email=%v", req.Email)

	return out
}

type UserLogin struct {
	BaseLoginReq
	Password string `json:"password"`
}

func (req *UserLogin) GetError() []error {
	var err []error
	err = append(err, req.BaseLoginReq.GetError()...)
	if e := validateStringWithLen(req.Password, "password", MaxEmailLen); e != nil {
		err = append(err, e)
	}

	return err
}

func (req *UserLogin) String() string {
	return req.BaseLoginReq.String()
}

type GoogleLogin struct {
	BaseLoginReq
}

func (req *GoogleLogin) GetError() []error {
	var err []error
	err = append(err, req.BaseLoginReq.GetError()...)

	return err
}

func (req *GoogleLogin) String() string {
	return req.BaseLoginReq.String()
}

type UserLoginResponse struct {
	Code              int64  `json:"code"`
	Message           string `json:"message"`
	ErrorMessage      string `json:"-"` // only to be used in html
	IsAccountVerified bool   `json:"isAccountVerified"`
	UserID            int64  `json:"-"`
}

type GenericResponse struct {
	ErrorMessage string `json:"-"` // only to be used in html
}

type UserLogout struct {
	MsgID    string          `json:"msgID"`
	UserID   int64           `json:"userID"`
	UserType public.UserType `json:"userType"`
}

func (req *UserLogout) GetError() []error {
	var err []error
	req.UserType = public.UTRegular
	return err
}

func (req *UserLogout) String() string {
	out := fmt.Sprintf("msgID=%v", req.MsgID)
	out += fmt.Sprintf(", userID=%v", req.UserID)
	out += fmt.Sprintf(", userType=%v", req.UserType)
	return out
}

type UserLogoutResponse struct {
	Status string `json:"status"`
}
