package dto

import (
	"fmt"
	"strings"

	public "bitbucket/go_chope/chope-assignment/commons/usertypes"
)

const (
	maxLen = 64
)

type UserRegister struct {
	BaseMetaReq
	MsgID     string `json:"msgID"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	Password  string `json:"password"`

	// Internal use
	Avatar           string          `json:"-"`
	IsEmailVerfied   bool            `json:"-"`
	UserType         public.UserType `json:"-"`
	IsThirpartyLogin bool            `json:"-"`
}

func (req *UserRegister) GetError() []error {
	err := req.BaseMetaReq.GetError()
	req.UserType = public.UTRegular

	// if e := validateStringWithLen(req.FirstName, "firstName", MaxNameLen); e != nil {
	// 	err = append(err, e)
	// }
	// if e := validateStringWithLen(req.LastName, "lastName", MaxNameLen); e != nil {
	// 	err = append(err, e)
	// }

	if e := validateStringWithLen(req.Email, "email", MaxEmailLen); e != nil {
		err = append(err, e)
	}

	if e := validateStringWithLen(req.Password, "password", MaxEmailLen); e != nil {
		err = append(err, e)
	}

	req.Email = strings.ToLower(req.Email)

	return err
}

func (req *UserRegister) String() string {
	out := req.BaseMetaReq.String() +
		fmt.Sprintf("msgID=%v", req.MsgID) +
		fmt.Sprintf(", firstName=%v", req.FirstName) +
		fmt.Sprintf(", lastName=%v", req.LastName) +
		fmt.Sprintf(", email=%v", req.Email)
	return out
}

type UserRegisterResponse struct {
	Status  string `json:"status,omitempty"`
	Code    int64  `json:"code"`
	Message string `json:"message,omitempty"`
	UserID  int64  `json:"-"`
}
