package dto

import (
	"errors"
	"fmt"
	"strings"

	public "bitbucket/go_chope/chope-assignment/commons/usertypes"
)

const (
	MaxPasswordLen = 32
	MaxEmailLen    = 32
	MaxMobileLen   = 32
	MaxNameLen     = 32
	MsgIDLen       = 32
)

const (
	alphabeticStringPattern   = `[a-zA-Z0-9_]+`
	alphaNumericStringPattern = `[a-zA-Z0-9_]+`
)

type BaseMetaReq struct {
	Platform   string `json:"platform"`
	AppVersion string `json:"appVersion"`
	Lang       string `json:"lang"`
}

func (req *BaseMetaReq) GetError() []error {
	var err []error
	return err
}

func (r *BaseMetaReq) String() string {
	out := fmt.Sprintf("platform=%s", r.Platform)
	out += fmt.Sprintf(", appVersion=%v", r.AppVersion)
	out += fmt.Sprintf(" ,lang=%s", r.Lang)

	return out
}

type BaseReq struct {
	MsgID    string          `json:"msgID"`
	UserID   int64           `json:"userID"`
	UserType public.UserType `json:"userType"`
}

func (r *BaseReq) GetError() []error {
	var err []error
	if r.UserID <= 0 {
		err = append(err, fmt.Errorf("invalid userID"))
	}

	if !r.UserType.IsValid() {
		err = append(err, fmt.Errorf("invalid userType"))
	}
	return err
}

func (r *BaseReq) String() string {
	out := fmt.Sprintf("msgID=%s, ", r.MsgID)
	out += fmt.Sprintf("userID=%v, ", r.UserID)
	out += fmt.Sprintf("userType=%s, ", r.UserType)
	return out
}

type BaseRsp struct {
	Code    int64  `json:"code"`
	Message string `json:"message"`
}

func String(errs []error) string {
	str := []string{}
	for _, err := range errs {
		str = append(str, fmt.Sprintf("%v", err))
	}
	return strings.Join(str, ",")
}

type BasicAuth struct {
}

func validateStringWithLen(value, key string, maxLen int) error {
	if len(value) <= 0 {
		return errors.New(fmt.Sprintf("missing %s", key))
	}

	if len(value) > maxLen {
		return errors.New(fmt.Sprintf("%s too long", key))
	}
	return nil
}

func validateUserType(typ string) error {
	if _, ok := public.GetUserTypeByName(typ); ok {
		return nil
	}

	return errors.New(fmt.Sprintf("invalid or missing userType=%s", typ))
}
