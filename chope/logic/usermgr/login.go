package usermgr

import (
	"errors"
	"log"
	"strings"

	"bitbucket/go_chope/chope-assignment/chope/storage"
	"bitbucket/go_chope/chope-assignment/commons/mysql"
)

func GetUserByEmail(email string) (*storage.User, error) {
	conditions := []mysql.Condition{
		mysql.EqualTo("Email", strings.ToLower(email)),
	}
	users, err := storage.UserDao.FindListConditions(conditions...)
	if err != nil {
		if err == mysql.ErrNoData {
			log.Printf("failed to find user,email=%v, err=%+v", email, err)
			return nil, err
		}
		log.Printf("failed to find user,email=%v, err=%+v", email, err)
		return nil, errors.New("db error while searching user")
	}

	return users[0], nil
}

func GetUserByID(id int64) (*storage.User, error) {
	conditions := []mysql.Condition{
		mysql.EqualTo("ID", id),
	}
	users, err := storage.UserDao.FindListConditions(conditions...)
	if err != nil {
		if err == mysql.ErrNoData {
			log.Printf("failed to find user,id=%v", id)
			return nil, err
		}
		log.Printf("failed to find user,id=%v, err=%+v", id, err)
		return nil, errors.New("db error while searching user")
	}

	return users[0], nil
}
