package usermgr

import (
	"log"
	"net/http"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"

	"bitbucket/go_chope/chope-assignment/chope/common"
	"bitbucket/go_chope/chope-assignment/chope/dto"
	"bitbucket/go_chope/chope-assignment/chope/storage"
	"bitbucket/go_chope/chope-assignment/commons/mysql"
	"bitbucket/go_chope/chope-assignment/commons/uuidgo"
)

func Register(req *dto.UserRegister, isAdmin bool) (*dto.UserRegisterResponse, *common.ApiError) {
	//Generate "hash" to store from user password
	log.Printf("[logic]register request received=%+v", req)

	conditions := []mysql.Condition{
		mysql.EqualTo("Email", strings.ToLower(req.Email)),
	}
	users, err := storage.UserDao.FindListConditions(conditions...)
	if err != nil && err != mysql.ErrNoData {
		log.Printf("error while finding user, err=%+v", err)
		return nil, common.ErrorInternalServer("db error while finding user")
	}
	userOld := users[0]

	if userOld != nil {
		log.Printf("existing user found, user=%+v", userOld)
		return nil, common.ErrorConflict("user already registered, try with different email")
	}

	userType := req.UserType

	hash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Printf("failed to encrypt password, err=%+v", err)
		return nil, common.ErrorInternalServer("failed to encrypt password")
	}

	newUser := &storage.User{}
	if !req.IsThirpartyLogin {
		newUser.Password = string(hash)
	}
	newUser.FirstName = req.FirstName
	newUser.LastName = req.LastName
	newUser.Email = req.Email
	newUser.Avatar = req.Avatar
	newUser.IsEmailVerified = req.IsEmailVerfied
	newUser.UserType = userType
	newUser.EmailVerificaionCode = uuidgo.NewUUID()
	newUser.EmailVerificationExpiresAt = time.Now().Add(time.Hour * 24).UTC()

	log.Printf("save new user=%+v", newUser)
	id, errSave := storage.UserDao.Save(newUser)
	if errSave != nil {
		log.Printf("failed to save new user to DB, err=%+v", errSave)
		return nil, common.ErrorInternalServer("db error while saving new user")
	}

	// Send an email for email address verification

	return &dto.UserRegisterResponse{
		Status:  "success",
		Code:    http.StatusOK,
		UserID:  id,
		Message: "Please check your email and complete email verification",
	}, nil

}
