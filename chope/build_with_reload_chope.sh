EC2_USERNAME=ubuntu
EC2_IP_SUFFIX=".ap-southeast-1.compute.amazonaws.com"
AWS_EC2_ADDR=""
EC2_ENV="stg"

EC2_IP_STG="54-255-165-196"

# Default stg IP
EC2_IP=$EC2_IP_STG
AWS_SSH_KEY="aws-key-pair-25Feb2020-asia-pacific-SG.pem"

# complete address
AWS_EC2_ADDR="${EC2_USERNAME}@ec2-${EC2_IP}${EC2_IP_SUFFIX}"
SSH_KEY_PATH="~/.ssh/${AWS_SSH_KEY}"

echo 
echo "Using ssh key... $SSH_KEY_PATH"

#######################
# Build info
#######################
BINARY_NAME=aws-binary.chope
echo
echo "Deleting old binary file..."
rm -f $BINARY_NAME

echo "building..."
GOOS=linux GOARCH=amd64 go build -o $BINARY_NAME

echo "Transferring binary '$BINARY_NAME' to $EC2_ENV instance ec2-${EC2_IP}"

scp -i $SSH_KEY_PATH $BINARY_NAME $AWS_EC2_ADDR:
scp -i $SSH_KEY_PATH ./templates/* -r $AWS_EC2_ADDR:templates/
