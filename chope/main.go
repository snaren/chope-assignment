package main

import (
	"log"

	"github.com/gorilla/mux"

	"bitbucket/go_chope/chope-assignment/chope/config"
	"bitbucket/go_chope/chope-assignment/chope/server"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Printf("app config=%+v", config.Config)
	config.Init()

	r := mux.NewRouter()
	v1 := r.PathPrefix("/v1").Subrouter()
	server.RegisterRoutes(v1)

	server.StartServer(r)

	return
}
