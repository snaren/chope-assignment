package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"bitbucket/go_chope/chope-assignment/commons/googleapis"
	"bitbucket/go_chope/chope-assignment/commons/redisapi"
)

const (
	envVar_APP_CONFIG_FILE = "APP_CONFIG_FILE"
	envVar_SESSION_KEY     = "SESSION_KEY"
	envVar_MYSQL_USER      = "MYSQL_USER"
	envVar_MYSQL_PASSWORD  = "MYSQL_PASSWORD"
)

type ServerConfig struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

type MysqlConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	DbName   string `json:"dbName"`
}

/************************/

type AppConfig struct {
	AppMode                    string                `json:"appMode"`
	SessionKey                 string                `json:"sessionKey"`
	Server                     ServerConfig          `json:"server"`
	ServerDomainAddr           string                `json:"serverDomainAddr"`
	Mysql                      MysqlConfig           `json:"mysqlConnectionInfo"`
	Redis                      *redisapi.RedisConfig `json:"redis"`
	FirebaseAdminSdkConfigFile string                `json:"firebaseAdminSdkConfigPath"`
}

var (
	Config            = &AppConfig{}
	WerbServerVersion = "v1.0.59"

	RedisClient    *redisapi.RedisClientImpl
	FireBaseClient *googleapis.FirebaseSdk
)

func Init() {
	err := loadConfig(Config)
	if err != nil {
		panic(fmt.Sprintf("failed to load config. err=%+v", err))
	}

	RedisClient = redisapi.NewClient(Config.Redis)
	if RedisClient == nil {
		log.Printf("failed to create redis client, config=%+v", Config.Redis)
	}

	FireBaseClient = googleapis.InitFirebaseSDK(Config.FirebaseAdminSdkConfigFile)
	if FireBaseClient == nil {
		log.Printf("failed to initialize firebase sdk, filePath=%v, err=%+v", Config.FirebaseAdminSdkConfigFile, err)
	}

	log.Printf("app config=%+v", Config)
}

func loadConfig(appConfig *AppConfig) error {
	fileAbsoluteName := os.Getenv(envVar_APP_CONFIG_FILE)
	err := readJSON(fileAbsoluteName, appConfig)
	if err != nil {
		log.Printf("failed to parse json config, filePath=%s, err=%+v", fileAbsoluteName, err)
		return err
	}

	appConfig.Mysql.User = os.Getenv(envVar_MYSQL_USER)
	if len(appConfig.Mysql.User) <= 0 {
		panic("empty " + envVar_MYSQL_USER)
	}

	appConfig.Mysql.Password = os.Getenv(envVar_MYSQL_PASSWORD)
	if len(appConfig.Mysql.Password) <= 0 {
		panic("empty " + envVar_MYSQL_PASSWORD)
	}

	return nil
}

func readJSON(fileName string, appConfig *AppConfig) error {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Printf("failed to read config file, filePath=%s, err=%+v", fileName, err)
		return err
	}
	return json.Unmarshal(file, appConfig)
}
