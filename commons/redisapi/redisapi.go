package redisapi

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/go-redis/redis"
)

type RedisConfig struct {
	Endpoint string `json:"endpoint"`
}

/**************************************
Redis Client
**************************************/
type RedisClient interface {
	LPush(ctx context.Context, key string, value ...interface{}) error
	LRange(ctx context.Context, key string, start, end int64) ([]string, error)
}

type RedisClientImpl struct {
	client *redis.Client
}

func NewClient(config *RedisConfig) *RedisClientImpl {
	if config == nil {
		panic("failed to initilize redis client, nil config")
	}

	client := redis.NewClient(&redis.Options{
		Addr:         config.Endpoint, // use default Addr
		Password:     "",              // no password set
		DB:           0,               // use default DB
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	})

	ctx := context.Background()
	pong, err := client.Ping(ctx).Result()
	if err != nil {
		log.Printf("failed to ping redis, err=%+v", err)
		return nil
	}
	log.Println(pong, err)
	return &RedisClientImpl{
		client: client,
	}
}

func (c *RedisClientImpl) LPush(ctx context.Context, key string, value ...interface{}) error {
	result, err := c.client.LPush(ctx, key, value...).Result()
	if err != nil {
		fmt.Println("redis set failed:", err)
		return err
	}
	log.Printf("LPush success, key=%v, result=%+v", key, result)
	return nil
}

func (c *RedisClientImpl) LRange(ctx context.Context, key string, start, end int64) ([]string, error) {
	result, err := c.client.LRange(ctx, key, start, end).Result()
	log.Printf("LRange key=%v, response=%+v, err=%+v", key, result, err)
	if err == redis.Nil {
		log.Printf("failed to execute redis LRange,err=%+v", err)
		return nil, err
	}

	return result, nil
}
