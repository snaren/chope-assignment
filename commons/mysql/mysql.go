package mysql

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"reflect"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

const (
	tagSqlColumn = "sql-col"
	tagSqlInsert = "sql-insert"
	tagSqlUpdate = "sql-update"
	tagSqlWhere  = "sql-where"

	OrderByASC  = "ASC"
	OrderByDESC = "DESC"
)

type Entity interface {
	GetTableName() string
	New() Entity
}

type MysqlConnInfo struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	DbName   string `json:"dbName"`
}

func (builder *MySqlMgr) BuildUpdateQuery(old, new interface{}) string {
	currObj := reflect.ValueOf(old).Elem()
	newObj := reflect.ValueOf(new).Elem()
	updatableCols := []string{}
	currValues := []interface{}{}
	newValues := []interface{}{}
	whereParams := []interface{}{}
	whereCols := []string{}

	// Get columns to put in SET clause of update statement
	count := 1
	for structName, sqlName := range builder.ColInfo.UpdateStructToSqlColName {
		log.Printf("\n update col index=%v key, val = %v, %v", count, structName, sqlName)
		updatableCols = append(updatableCols, sqlName)
		currValues = append(currValues, currObj.FieldByName(structName).Interface())
		newValues = append(newValues, newObj.FieldByName(structName).Interface())
		count++
	}

	// Get columns to put in WHERE clause of update statement
	count = 1
	for structName, sqlName := range builder.ColInfo.WhereStructToSqlColName {
		if structName == "RoutePayload" {
			continue
		}

		whereParams = append(whereParams, currObj.FieldByName(structName).Interface())
		whereCols = append(whereCols, sqlName)
		log.Printf("\n update whr col index=%v key, val, param val = %v, %v, %v", count, structName, sqlName, currObj.FieldByName(structName))
		count++
	}

	cols := strings.Join(updatableCols, " = ?, ") + " = ?"
	whereClause := strings.Join(whereCols, " = ? AND ") + " = ?"

	query := "UPDATE " + builder.EntityObj.GetTableName() + " " + "SET " + cols + " WHERE " + whereClause + ";"
	builder.queryinfo.whereParams = append(builder.queryinfo.whereParams, whereParams...)
	builder.queryinfo.params = append(builder.queryinfo.params, newValues...)

	return query
}

func (builder *MySqlMgr) BuildInsertQuery(obj interface{}) (string, []interface{}) {
	val := reflect.ValueOf(obj).Elem()
	insertCols := []string{}
	plcaeHolder := []string{}
	params := []interface{}{}

	for structName, sqlName := range builder.ColInfo.InsertStructToSqlColName {
		insertCols = append(insertCols, sqlName)
		plcaeHolder = append(plcaeHolder, "?")
		params = append(params, val.FieldByName(structName).Interface())
	}

	valPlaceHolderStr := strings.Join(plcaeHolder, ", ")
	insert := "INSERT INTO " +
		builder.EntityObj.GetTableName() +
		" (" +
		strings.Join(insertCols, ", ") +
		") VALUES (" +
		valPlaceHolderStr + ");"

	return insert, params
}

func (builder *MySqlMgr) FindListConditions(conditions ...Condition) ([]Entity, error) {

	//TODO implement better
	builder.queryinfo = sqlQueryInfo{}
	q, qParams, err := BuildLoadListQueryFromConditions(builder, conditions...)
	if err != nil {
		return nil, err
	}

	log.Printf("find query = %v,", q)
	rows, err := query(builder.Config.DB, q, qParams...)
	if err != nil {
		log.Printf("query failed,query = %v, err = %+v", q, err)
		return nil, errors.New("query failed")
	}
	defer rows.Close()
	count := 0

	result := []Entity{}

	var (
		structPtr   Entity
		fieldsAddrs []interface{}
	)
	for rows.Next() {
		fieldsAddrs = []interface{}{}
		structPtr = builder.EntityObj.New()
		obj := reflect.ValueOf(structPtr).Elem()
		for i := 0; i < len(builder.ColInfo.ColSqlAll); i++ {
			col := builder.ColInfo.ColSqlAll[i]
			structName := builder.ColInfo.ColsSqlToStructName[col]
			id := builder.ColInfo.ColsStructNameToID[structName]
			// log.Printf("idx=%v, col = %v, stName = %v", i, col, structName)

			// selectCols = append(selectCols, col)
			fieldsAddrs = append(fieldsAddrs, obj.Field(id).Addr().Interface())
		}
		err = rows.Scan(fieldsAddrs...)
		if err == sql.ErrNoRows {
			log.Printf("query failed ,err = %+v", err)
			return nil, errors.New("query failed")
		}
		count++
		result = append(result, structPtr)
		// log.Printf("\nDB list result = %+v, obj=%+v", result, structPtr)
	}
	log.Printf("\nDB list result = %+v", result)

	if count == 0 {
		log.Println("no user found")
		return nil, ErrNoData
	}

	err = rows.Err()
	if err != nil {
		log.Printf("scan failed ,err = %+v", err)
		return nil, errors.New("scan failed")
	}

	return result, nil

}

func (builder *MySqlMgr) Save(obj interface{}) (int64, error) {
	query, params := builder.BuildInsertQuery(obj)

	errPing := builder.Config.DB.Ping()
	if errPing != nil {
		log.Println("failed to ping DB before insert")
		return 0, errPing
	}

	insert, err := exec(builder.Config.DB, query, params...)
	if err != nil {
		log.Printf("failed to insert to DB")
		return 0, err
	}

	id, err := insert.LastInsertId()
	if err != nil {
		log.Println("failed to get LastInsertId")
		return 0, err
	}

	count, errr := insert.RowsAffected()
	if errr != nil {
		log.Println("failed to get RowsAffected")
		return 0, errr
	}
	log.Printf("%d rows affected, saved row id=%d", count, id)
	return id, nil
}

func (builder *MySqlMgr) UpdateNew(currObj, newObj interface{}) error {
	if currObj == nil || newObj == nil {
		log.Printf("nil curr or new obj, curr=%v, new=%v", currObj, newObj)
		return errors.New("nil curr or new obj")
	}
	builder.queryinfo.whereParams = []interface{}{}
	builder.queryinfo.params = []interface{}{}
	query := builder.BuildUpdateQuery(currObj, newObj)
	params := []interface{}{}
	params = append(params, builder.queryinfo.params...)
	params = append(params, builder.queryinfo.whereParams...)

	// log.Printf("update new. query:%+v:, len=%v, whereparams=%v, len=%v,params=%v", query, len(builder.queryinfo.whereParams), builder.queryinfo.whereParams, len(builder.queryinfo.params), builder.queryinfo.params)
	// log.Printf("update new. query:%+v:, len=%v, updateparams=%v", query, len(builder.queryinfo.params), builder.queryinfo.params)
	update, err := exec(builder.Config.DB, query, params...)
	if err != nil {
		log.Printf("failed to update db row")
		return err
	}

	count, errr := update.RowsAffected()
	if errr != nil {
		log.Println("failed to get RowsAffected for update")
		return errr
	}
	log.Printf("%d rows updated", count)
	return nil
}

func BuildLoadListQueryFromConditions(dbmgr *MySqlMgr, conditions ...Condition) (string, []interface{}, error) {
	selectCols, whereClause, err := buildLoadBaseQueryFromConditions(dbmgr, conditions...)
	if err != nil {
		log.Printf("find query = %v,", "")
		return "", nil, err
	}

	if len(whereClause) == 0 {
		log.Printf("nil where clause")
		return "", nil, errors.New("nil where clause")
	}

	query := "SELECT " + strings.Join(selectCols, ", ") + " FROM " + dbmgr.EntityObj.GetTableName() +
		" where " + whereClause

	if dbmgr.queryinfo.orderBy != "" {
		query += " ORDER BY " + dbmgr.queryinfo.orderByCol + " " + dbmgr.queryinfo.orderBy
	} else {
		query += " ORDER BY `id` DESC" //default
	}

	if dbmgr.queryinfo.limit > 0 && dbmgr.queryinfo.offset > 0 {
		query += " LIMIT ?, ?"
		dbmgr.queryinfo.params = append(dbmgr.queryinfo.params, dbmgr.queryinfo.offset)
		dbmgr.queryinfo.params = append(dbmgr.queryinfo.params, dbmgr.queryinfo.limit)
	} else if dbmgr.queryinfo.limit > 0 {
		query += " LIMIT ?"
		dbmgr.queryinfo.params = append(dbmgr.queryinfo.params, interface{}(dbmgr.queryinfo.limit))
	}

	query += ";"

	log.Printf("find query = %v, params=%v", query, dbmgr.queryinfo.params)
	return query, dbmgr.queryinfo.params, nil
}

func buildLoadBaseQueryFromConditions(dbmgr *MySqlMgr, conditions ...Condition) ([]string, string, error) {
	if len(conditions) == 0 {
		return nil, "", fmt.Errorf("build query failed, no condition specified")
	}

	whereClause := []string{}
	for _, condition := range conditions {
		where, err := condition(dbmgr)
		if err != nil {
			return nil, "", fmt.Errorf("build query failed,err=%+v", err)
		}
		if where != "" {
			whereClause = append(whereClause, where)
		}
	}

	whereStr := strings.Join(whereClause, " AND ")

	obj := reflect.ValueOf(dbmgr.EntityObj.New()).Elem()
	typ := reflect.TypeOf(dbmgr.EntityObj).Elem()

	selectCols := []string{}
	queryParams := make([]interface{}, obj.NumField())
	for i := 0; i < obj.NumField(); i++ {

		selectCols = append(selectCols, dbmgr.ColInfo.ColsStructToSqlName[typ.Field(i).Name])
		queryParams[i] = obj.Field(i).Addr().Interface()
	}

	return selectCols, whereStr, nil
}
