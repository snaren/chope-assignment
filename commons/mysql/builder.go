package mysql

import (
	"context"
	"database/sql"
	"log"
)

type MysqlConfig struct {
	DB *sql.DB
}

func GetDao(config *MysqlConnInfo, entity Entity) *MySqlMgr {
	colInfo := ColumnInfo{}
	buildColumnInfo(&colInfo, entity)
	conn, _ := connect(config.User, config.Password, config.Host, config.DbName)
	return &MySqlMgr{
		EntityObj: entity,
		Config: &MysqlConfig{
			DB: conn,
		},
		ColInfo: colInfo,
	}
}

type ColumnInfo struct {
	ColsStructToSqlName      map[string]string
	ColsSqlToStructName      map[string]string
	UpdateStructToSqlColName map[string]string
	WhereStructToSqlColName  map[string]string
	InsertStructToSqlColName map[string]string
	ColsStructNameToID       map[string]int
	ColSqlAll                []string
	IsColInfoSet             bool
}

type sqlQueryInfo struct {
	where       string
	whereParams []interface{}
	params      []interface{}
	offset      int64
	limit       int64
	orderByCol  string
	orderBy     string
}

type MySqlMgr struct {
	EntityObj Entity
	Config    *MysqlConfig
	ColInfo   ColumnInfo
	queryinfo sqlQueryInfo
}

func exec(db *sql.DB, query string, args ...interface{}) (sql.Result, error) {
	err := db.Ping()
	if err == nil {
		return db.ExecContext(context.Background(), query, args...)
	}

	log.Printf("db exec first ping failed, err=%+v", err)

	if err := db.Ping(); err != nil {
		log.Printf("db exec second ping failed, err=%+v", err)
		return nil, err
	}
	return db.ExecContext(context.Background(), query, args...)
}

func query(db *sql.DB, query string, args ...interface{}) (*sql.Rows, error) {
	err := db.Ping()
	if err == nil {
		return db.Query(query, args...)
	}
	log.Printf("db query first ping failed, err=%+v", err)

	if err := db.Ping(); err != nil {
		log.Printf("db quwery second ping failed, err=%+v", err)
		return nil, err
	}

	return db.Query(query, args...)
}
