package mysql

import "errors"

var (
	ErrNoData = errors.New("no result found")
)
