package mysql

import (
	"fmt"
	"strings"
)

type Condition func(bulder *MySqlMgr) (where string, err error)

func ContainedIn(field string, vals ...interface{}) Condition {
	return func(builder *MySqlMgr) (string, error) {
		column, ok := builder.ColInfo.ColsStructToSqlName[field]
		if !ok {
			return "", fmt.Errorf("no column found with struct field name '%s'", field)
		}

		placeholder := []string{}
		values := []interface{}{}
		for _, val := range vals {
			placeholder = append(placeholder, "?")
			values = append(values, val)
			// fmt.Printf("placeholder=%v", placeholder)
		}

		builder.queryinfo.params = append(builder.queryinfo.params, vals...)
		fmt.Printf("q=%v, params=%v", fmt.Sprintf("%s in ("+strings.Join(placeholder, ", ")+")", column), builder.queryinfo.params)
		return fmt.Sprintf("%s in ("+strings.Join(placeholder, ", ")+")", column), nil
	}
}

func EqualTo(field string, val interface{}) Condition {
	return func(builder *MySqlMgr) (string, error) {
		column, ok := builder.ColInfo.ColsStructToSqlName[field]
		if !ok {
			return "", fmt.Errorf("no column found with struct field name '%s'", field)
		}

		builder.queryinfo.params = append(builder.queryinfo.params, val)
		return fmt.Sprintf("%s = ?", column), nil
	}
}

func NotEqualTo(field string, val interface{}) Condition {
	return func(builder *MySqlMgr) (string, error) {
		column, ok := builder.ColInfo.ColsStructToSqlName[field]
		if !ok {
			return "", fmt.Errorf("no column found with struct field name '%s'", field)
		}

		builder.queryinfo.params = append(builder.queryinfo.params, val)
		return fmt.Sprintf("%s != ?", column), nil
	}
}

func GreaterThan(field string, val interface{}) Condition {
	return func(builder *MySqlMgr) (string, error) {
		column, ok := builder.ColInfo.ColsStructToSqlName[field]
		if !ok {
			return "", fmt.Errorf("no column found with struct field name '%s'", field)
		}

		builder.queryinfo.params = append(builder.queryinfo.params, val)
		return fmt.Sprintf("%s > ?", column), nil
	}
}

func GreaterThanEqualTo(field string, val interface{}) Condition {
	return func(builder *MySqlMgr) (string, error) {
		column, ok := builder.ColInfo.ColsStructToSqlName[field]
		if !ok {
			return "", fmt.Errorf("no column found with struct field name '%s'", field)
		}

		builder.queryinfo.params = append(builder.queryinfo.params, val)
		return fmt.Sprintf("%s >= ?", column), nil
	}
}

func LessThan(field string, val interface{}) Condition {
	return func(builder *MySqlMgr) (string, error) {
		column, ok := builder.ColInfo.ColsStructToSqlName[field]
		if !ok {
			return "", fmt.Errorf("no column found with struct field name '%s'", field)
		}

		builder.queryinfo.params = append(builder.queryinfo.params, val)
		return fmt.Sprintf("%s < ?", column), nil
	}
}

func LessThanEqualTo(field string, val interface{}) Condition {
	return func(builder *MySqlMgr) (string, error) {
		column, ok := builder.ColInfo.ColsStructToSqlName[field]
		if !ok {
			return "", fmt.Errorf("no column found with struct field name '%s'", field)
		}

		builder.queryinfo.params = append(builder.queryinfo.params, val)
		return fmt.Sprintf("%s <= ?", column), nil
	}
}

func And(conditions ...Condition) Condition {
	return func(builder *MySqlMgr) (string, error) {
		if len(conditions) == 0 {
			return "", nil
		}

		q := []string{}
		for _, condition := range conditions {
			subCondition, err := condition(builder)
			if err != nil {
				return "", err
			}
			q = append(q, subCondition)
		}

		query := "(" + strings.Join(q, " AND ") + ")"
		return query, nil
	}
}

func Or(conditions ...Condition) Condition {
	return func(builder *MySqlMgr) (string, error) {
		if len(conditions) == 0 {
			return "", nil
		}

		q := []string{}
		for _, condition := range conditions {
			subCondition, err := condition(builder)
			if err != nil {
				return "", err
			}
			q = append(q, subCondition)
		}

		query := "(" + strings.Join(q, " OR ") + ")"
		return query, nil
	}
}

func Limit(offset, limit int64) Condition {
	return func(builder *MySqlMgr) (string, error) {
		builder.queryinfo.offset = offset
		builder.queryinfo.limit = limit
		return "", nil
	}
}

func OrderBy(field, orderBy string) Condition {
	return func(builder *MySqlMgr) (string, error) {
		column, ok := builder.ColInfo.ColsStructToSqlName[field]
		if !ok {
			return "", fmt.Errorf("no column found with struct field name '%s'", field)
		}

		if orderBy != OrderByASC && orderBy != OrderByDESC {
			return "", fmt.Errorf("invalid order='%s' for orderby clause,must be uppercase ASC/DESC", orderBy)
		}
		builder.queryinfo.orderByCol = column
		builder.queryinfo.orderBy = orderBy
		return "", nil
	}
}
