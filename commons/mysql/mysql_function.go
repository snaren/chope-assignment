package mysql

import (
	"database/sql"
	"log"
	"reflect"
)

func buildColumnInfo(colInfo *ColumnInfo, entity interface{}) {
	if reflect.ValueOf(entity).Kind() != reflect.Ptr || reflect.TypeOf(entity).Elem().Kind() != reflect.Struct {
		log.Println("[SetColumnInfo]failed, struct ptr type expected, received=" + reflect.ValueOf(entity).Kind().String())
	}

	typ := reflect.TypeOf(entity).Elem()

	structToSqlName := make(map[string]string)
	sqlToStructName := make(map[string]string)
	updateColumnInfo := make(map[string]string)
	whereColumnInfo := make(map[string]string)
	insertColumnInfo := make(map[string]string)
	colNameToID := make(map[string]int)
	allColumnSlice := []string{}

	for i := 0; i < typ.NumField(); i++ {
		field := typ.Field(i)
		col := field.Tag.Get(tagSqlColumn)
		if len(col) == 0 {
			continue //no column for this struct field
		}

		structToSqlName[field.Name] = "`" + col + "`"
		sqlToStructName["`"+col+"`"] = field.Name
		colNameToID[field.Name] = i

		allColumnSlice = append(allColumnSlice, "`"+col+"`")

		colInsertTag := field.Tag.Get(tagSqlInsert)
		if colInsertTag == "" || colInsertTag == "true" {
			insertColumnInfo[field.Name] = col
		}

		colUpdateTag := field.Tag.Get(tagSqlUpdate)
		if colUpdateTag == "" || colUpdateTag == "true" {
			updateColumnInfo[field.Name] = col
		}

		colWhereTag := field.Tag.Get(tagSqlWhere)
		if colWhereTag == "" || colWhereTag == "true" {
			whereColumnInfo[field.Name] = col
		}
	}

	colInfo.ColSqlAll = allColumnSlice
	colInfo.ColsStructToSqlName = structToSqlName
	colInfo.ColsSqlToStructName = sqlToStructName
	colInfo.UpdateStructToSqlColName = updateColumnInfo
	colInfo.WhereStructToSqlColName = whereColumnInfo
	colInfo.InsertStructToSqlColName = insertColumnInfo
	colInfo.ColsStructNameToID = colNameToID
	colInfo.IsColInfoSet = true

}

func connect(user, password, ip, dbName string) (*sql.DB, error) {
	db, err := sql.Open("mysql", user+":"+password+"@tcp("+ip+")/"+dbName+"?parseTime=true")
	if err != nil {
		log.Printf("db connect failed, dbName=%s, err=%+v", dbName, err)
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		log.Printf("db ping failed, err=%+v", err)
		return nil, err
	}
	log.Printf("db ping success")
	return db, nil

}
