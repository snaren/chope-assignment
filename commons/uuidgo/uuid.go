package uuidgo

import (
	"fmt"
	"strings"

	uuid "github.com/satori/go.uuid"
)

func NewUUID() string {
	newUuid, err := uuid.NewV4()
	if err != nil {
		fmt.Printf("failed to generate new v4 UUID, will generate v1. err=%+v", err)
		newUuid, err = uuid.NewV1()
		if err != nil {
			panic("failed to generate new v1 UUID, err=%+v")
		}
		return strings.Replace(newUuid.String(), "-", "", -1)
	}
	return strings.Replace(newUuid.String(), "-", "", -1)
}
