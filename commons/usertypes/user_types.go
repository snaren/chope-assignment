package usertypes

type UserType int64

const (
	UTInvalid UserType = iota
	UTRegular UserType = 1

	UTMax UserType = 2
)

var UserTypes map[string]UserType
var UserTypeToNameMap map[UserType]string

func init() {
	UserTypes = map[string]UserType{
		"Regular": UTRegular,
	}

	UserTypeToNameMap = map[UserType]string{
		UTRegular: "Regular",
	}
}

func (ut UserType) IsRegular() bool {
	return ut == UTRegular
}

func (ut UserType) IsValid() bool {
	return ut > UTInvalid && ut < UTMax
}

func (ut UserType) String() string {
	if str, ok := UserTypeToNameMap[ut]; ok {
		return str
	}
	return ""
}

func GetUserTypeByName(typ string) (UserType, bool) {
	if userType, ok := UserTypes[typ]; ok {
		return userType, true
	}
	return UTInvalid, false
}
