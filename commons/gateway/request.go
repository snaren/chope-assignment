package gateway

type ReqValidator interface {
	String() string
	GetError() []error
}

type BadError struct {
	Code int    `json:"code"`
	Msg  string `json:"message"`
}
