package gateway

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"reflect"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"

	public "bitbucket/go_chope/chope-assignment/commons/usertypes"
)

const (
	RequestID = "X-Request-ID"
)

type APIError struct {
	Code         int    `json:"code"`
	ErrorMessage string `json:"-"`
	Msg          string `json:"msg"`
}

const (
	templateUnAuthorizedAccess = "./templates/login_error.html"
	templateForbiddenAccess    = "./templates/login_error.html"
	templateServerGenericError = "./templates/index.html"
)

type AppHandler func(w http.ResponseWriter, req *http.Request, validReq ReqValidator) (*template.Template, interface{})

func HandleWeb(handler AppHandler, dto interface{}) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {

		if err := request.ParseForm(); err != nil {
			log.Printf("failed to parse form, err=%+v", err)
			writer.WriteHeader(http.StatusInternalServerError)
			resp, _ := json.Marshal(&APIError{
				Code: http.StatusInternalServerError,
				Msg:  "failed to parse form",
			})
			writer.Write(resp)
			return
		}

		dtoNew := reflect.New(reflect.TypeOf(dto).Elem()).Interface()

		decoder := json.NewDecoder(request.Body)
		if err := decoder.Decode(dtoNew); err != nil {
			log.Println("failed to parse req.body,err=", err)
		} else {
			log.Printf("parse body success:%+v:, address=%p\n", dtoNew, dtoNew)
		}

		parsePathVars(dtoNew, mux.Vars(request), request)
		log.Printf("req.Form:%+v, req.PostForm:%+v\n", request.Form, request.PostForm)
		errParse := fillPathVarsToDto(dtoNew, request.Form)
		if errParse != nil {
			log.Printf("parsing url failed, err=%+v", errParse)
			writer.WriteHeader(http.StatusBadRequest)
			resp, _ := json.Marshal(&APIError{Msg: fmt.Sprintf("%s", errParse)})
			writer.Write(resp)
			return
		}

		log.Printf("executing handler...")
		tmpl, data := handler(writer, request, dtoNew.(ReqValidator))
		writer.Header().Set("Content-Type", "text/html; charset=UTF-8")
		tmpl.Execute(writer, data)
	}

}

func HandleWebWithLogin(handler AppHandler, dto interface{}, authUserTypes ...public.UserType) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		var (
			session  *sessions.Session
			err      error
			userID   int64
			userType int64
		)

		log.Printf("http req=%+v", request)
		allowedUserType := []public.UserType{public.UTRegular}
		if len(authUserTypes) > 0 {
			allowedUserType = authUserTypes
		}
		for _, authType := range allowedUserType {
			session, userID, userType, err = getUserSessionData(request, authType)
			if err == nil {
				break
			}
		}

		if err != nil || session == nil || userID <= 0 || userType <= 0 || userType >= int64(public.UTMax) {
			log.Printf("failed to get user session. authentication fails,userID=%d, userType=%v, err=%+v", userID, userType, err)
			resp := &APIError{
				ErrorMessage: "Could not verify if you are logged in. Please login to continue",
			}
			writeHTMLResponse(writer, template.Must(template.ParseFiles(templateForbiddenAccess)), resp)
			return
		}

		log.Printf("get user session success...")
		dtoNew := reflect.New(reflect.TypeOf(dto).Elem()).Interface()

		// fmt.Printf("parsing form ...")
		if err := request.ParseForm(); err != nil {
			log.Printf("failed to parse form, err=%+v", err)
			resp := &APIError{
				ErrorMessage: "Server error:failed to parse form data in request: Please login to continue",
			}
			writeHTMLResponse(writer, template.Must(template.ParseFiles(templateForbiddenAccess)), resp)
			return
		}

		if err := json.NewDecoder(request.Body).Decode(dtoNew); err != nil {
			log.Println("failed to parse req.body,err=", err)
		}

		parsePathVars(dtoNew, mux.Vars(request), request)
		log.Printf("req.Form:%+v, req.PostForm:%+v\n", request.Form, request.PostForm)
		errParse := fillPathVarsToDto(dtoNew, request.Form)
		if errParse != nil {
			log.Printf("parsing url failed, err=%+v", errParse)
			resp := &APIError{
				ErrorMessage: ":failed to parse reques data: Please try again",
			}
			writeHTMLResponse(writer, template.Must(template.ParseFiles(templateForbiddenAccess)), resp)
			return
		}

		v := reflect.ValueOf(dtoNew).Elem()
		uidVal := v.FieldByName("UserID")
		if uidVal.CanSet() {
			uidVal.SetInt(userID)
		}

		utypeVal := v.FieldByName("UserType")
		if utypeVal.CanSet() {
			utypeVal.SetInt(userType)
		}

		log.Printf("executing handler...")
		tmpl, data := handler(writer, request, dtoNew.(ReqValidator))
		writeHTMLResponse(writer, tmpl, data)
	}
}

func writeHTMLResponse(w http.ResponseWriter, tmpl *template.Template, data interface{}) {
	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	tmpl.Execute(w, data)
}
