package gateway

import (
	"os"

	"github.com/gorilla/sessions"

	public "bitbucket/go_chope/chope-assignment/commons/usertypes"
)

const (
	storeRegular   = "session-users"
	storeProvider  = "session-providers"
	storeCorporate = "session-corporates"
	storeConceirge = "session-conceirge"

	userID = "userID"
)

type Authenticator interface {
	GetUserID() int64
}

var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	// key   = []byte(Key)
	store *sessions.CookieStore
)

func init() {
	store = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))
}

func GetStore() *sessions.CookieStore {
	return store
}

func GetStoreNameByUserType(ut public.UserType) string {
	switch ut {
	default:
		return storeRegular
	}
}
