package gateway

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"reflect"
	"strconv"

	"github.com/gorilla/sessions"

	public "bitbucket/go_chope/chope-assignment/commons/usertypes"
)

func getUserSessionData(r *http.Request, authUserType public.UserType) (*sessions.Session, int64, int64, error) {
	sessionKey := GetStoreNameByUserType(authUserType)
	session, err := GetStore().Get(r, sessionKey)
	if err != nil {
		log.Printf("failed to get user session, userType=%v, err=%+v", authUserType.String(), err)
		return nil, 0, 0, err
	}

	if session.IsNew {
		log.Printf("new session for userType=%s, auth Forbidden.", authUserType.String())
		return nil, 0, 0, errors.New("session not found")
	}

	if val, ok := session.Values["authenticated"]; !ok || !val.(bool) {
		log.Printf("user not authorized")
		return nil, 0, 0, errors.New("user not authorized as per session")
	}

	userID, uidOk := session.Values["userID"]
	if !uidOk {
		log.Printf("userID not found in session")
		return nil, 0, 0, errors.New("session not found")
	} else if userID.(int64) <= 0 {
		log.Printf("invalid userID found in session")
		return nil, 0, 0, errors.New("session not found")
	}

	userType, ok := session.Values["userType"]
	if !ok {
		log.Printf("userType not found in session")
		return nil, 0, 0, errors.New("session not found")
	} else if userType.(int64) <= int64(public.UTInvalid) || userType.(int64) >= int64(public.UTMax) {
		log.Printf("invalid userType found in session")
		return nil, 0, 0, errors.New("session not found")
	}

	uid, ok := userID.(int64)
	uType, ok := userType.(int64)
	log.Printf("userID = %v, userType=%v, session=%+v", uid, userType, session)

	return session, uid, uType, nil
}

func parsePathVars(object interface{}, m map[string]string, req *http.Request) {
	for k, v := range m {
		req.Form.Set(k, v)
	}
}

func fillPathVarsToDto(dto interface{}, values url.Values) error {
	v := reflect.ValueOf(dto).Elem()
	t := v.Type()

	if t.Kind() != reflect.Struct {
		log.Printf("fillPathVarsToDto failed, struct type dto expected")
		return fmt.Errorf("parse form failed")
	}

	for i := 0; i < t.NumField(); i++ {
		kindField := t.Field(i).Type.Kind()
		tag := t.Field(i).Tag.Get("json")
		if _, ok := values[tag]; !ok {
			continue // continue with next struct field
		}
		if !v.Field(i).CanSet() {
			log.Printf("can not set struct field=%+v", t.Field(i).Name)
			continue
		}

		switch kindField {
		case reflect.Int64, reflect.Int:
			// values is a map of string  key to slice values, like map[email:[emailParam emailParam2] userID:[12]]
			val, errParse := strconv.ParseInt(values[tag][0], 10, 64)
			if errParse != nil {
				err := fmt.Errorf("failed to parse path value=`%v` to int", values[tag][0])
				log.Println(err)
				return err
			}
			v.Field(i).SetInt(val)

		case reflect.String:

			v.Field(i).SetString(values[tag][0])
		case reflect.Slice:

			sliceType := t.Field(i).Type
			fmt.Printf("Slice Type %s:\n", sliceType)
			// Get the type of a single slice element
			sliceElementType := sliceType.Elem()

			if sliceElementType.Kind() == reflect.Ptr {
				for i, inValue := range values[tag] {
					// Create a new slice element
					newItem := reflect.New(sliceElementType.Elem())
					newItem.Elem().SetString(inValue)
					v.Field(i).Set(reflect.Append(v.Field(i), newItem))
				}
			} else {
				fmt.Printf("Slice items are not string, Type %s:\n", sliceType)
				break
			}

		default:
			log.Printf("invalid kind of path var, skipping pathParam=%v, kind=%+v", t.Field(i).Name, t.Field(i).Type.Kind())

		}
	}
	return nil
}
