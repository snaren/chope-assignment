package googleapis

import (
	"log"

	"golang.org/x/net/context"

	firebase "firebase.google.com/go"

	"firebase.google.com/go/auth"
	"google.golang.org/api/option"
)

var (
	opt    option.ClientOption
	client *auth.Client
)

type FirebaseSdk struct {
	AuthClient *auth.Client
}

var fireBaseSDK = &FirebaseSdk{}

func InitFirebaseSDK(configFile string) *FirebaseSdk {
	ctx := context.Background()

	log.Printf("initializing firebase sdk with file=%v\n", configFile)
	opt = option.WithCredentialsFile(configFile)
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		log.Printf("error creating new firebase app. err=%+v", err)
		return nil
	}

	// Get an auth client from the firebase.App
	fireBaseSDK.AuthClient, err = app.Auth(ctx)
	if err != nil {
		log.Printf("error getting firebase auth client, err=%+v", err)
		return nil
	}

	log.Printf("firebase sdk initialized successfully")

	return fireBaseSDK
}

func (sdk *FirebaseSdk) VerifyToken(idToken, email string) *GoogleProfileData {
	log.Printf("verifying email=%v, idtoken=%+v\n", email, idToken)

	/***** Verify token with google backend in server-to-server call here *****/
	// TODO: Uncomment below code after google console configuration fix

	// if sdk.AuthClient == nil {
	// 	log.Printf("nil auth client for firebase sdk.")
	// 	return nil
	// }

	// token, err := sdk.AuthClient.VerifyIDToken(ctx, string(idToken))
	// if err != nil {
	// 	log.Printf("error verifying id token, err=%+v\n", err)
	// 	return nil
	// }

	log.Printf("Verified id token successfully")

	profile := &GoogleProfileData{}

	// Fetch user profile from google server
	// TODO: Uncomment below code after google console configuration fix

	// u, err := sdk.AuthClient.GetUserByEmail(ctx, email)
	// if err != nil {
	// 	log.Printf("error getting user by email=%s, err=%v. Try getting userinfo full", email, err)
	// 	return nil
	// }

	log.Printf("Successfully fetch user profile from google")
	// profile.Email = u.Email
	// profile.FirstName = u.DisplayName
	// profile.IsEmailVerified = u.EmailVerified
	// profile.Picture = u.UserInfo.PhotoURL

	// if profile.Picture == "" && u.ProviderUserInfo[0] != nil {
	// 	profile.Picture = u.ProviderUserInfo[0].PhotoURL
	// }

	profile.Email = email

	log.Printf("successfully fetched user profile from firebase: %+v\n", profile)
	return profile
}
