package googleapis

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// Credentials which stores google ids.
type Credentials struct {
	Clientid     string `json:"cid"`
	Clientsecret string `json:"csecret"`
}

var conf = &oauth2.Config{
	Scopes: []string{
		"https://www.googleapis.com/auth/userinfo.profile",
		"https://www.googleapis.com/auth/userinfo.email", // You have to select your own scope from here -> https://developers.google.com/identity/protocols/googlescopes#google_sign-in
	},
	Endpoint: google.Endpoint,
}

type GoogleProfileData struct {
	Email           string `json:"email"`
	FirstName       string `json:"given_name"`
	LastName        string `json:"family_name"`
	IsEmailVerified bool   `json:"email_verified"`
	Picture         string `json:"picture"`
}
